import React from "react";

import MainComponent from "../renderer/Components/mainComponent"

const App = () => {
	return(
		<div className="App">
			<MainComponent />
		</div>
	);
}

export default App;
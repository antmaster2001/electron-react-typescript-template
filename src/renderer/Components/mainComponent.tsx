import React from "react";

const MainDisplay = () => {
	return (
		<div className="text-normal grid justify-items-center w-100 bg-grey">
			<div className="absolute inset-0 flex items-center justify-center">
				<h1>Welcome to React Electron with typescript</h1>
			</div>
		</div>
	);
}

export default MainDisplay;